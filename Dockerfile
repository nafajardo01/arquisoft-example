FROM golang:1.19 as builder

WORKDIR /app
COPY go.* ./

RUN go mod download

COPY *.go ./
RUN go build -o /hello-arquisoft

FROM gcr.io/distroless/base-debian11 AS build-release-stage

WORKDIR /

COPY --from=builder /hello-arquisoft /hello-arquisoft

EXPOSE 5000

ENTRYPOINT [ "/hello-arquisoft" ]