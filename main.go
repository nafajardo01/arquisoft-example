package main

import (
	"fmt"
	"io"
	"net/http"
)

func getRoot(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got / request\n")
	io.WriteString(w, "Arquisoft Root Page")
}

func getGreet(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got /arquisoft\n")
	io.WriteString(w, "Hello world from Arquisoft Add New Message Three!")
}

func getHelloFromRegistry(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got /registry\n")
	fmt.Printf("request: %v", &r)
	io.WriteString(w, "Hello world from image in registry")
}

func main() {
	fmt.Printf("starting server...\n")
	mux := http.NewServeMux()
	mux.HandleFunc("/", getRoot)
	mux.HandleFunc("/arquisoft", getGreet)
	mux.HandleFunc("/registry", getHelloFromRegistry)

	err := http.ListenAndServe(":5000", mux)
	if err == http.ErrServerClosed {
		fmt.Printf("server closed\n")
	}
	if err != nil {
		fmt.Printf("error starting server: %s\n", err)
	}

}
